pkgname=chroot-pacman
pkgver=5.1.1
pkgrel=1
arch=('x86_64' 'i686' 'aarch64' 'armv7h' 'armv5tel' 'mips64' 'mips' 'ppc64le' 'ppc64' 'ppc')
source=(https://sources.archlinux.org/other/pacman/pacman-$pkgver.tar.gz
	pacman.conf
	makepkg.conf)

prepare() {
	cd pacman-$pkgver
	sed -i -e '/x-cpio/s@)@|*application/x-empty*)@' scripts/makepkg.sh.in
	sed -i -e 's/EUID == 0/EUID == -1/' scripts/makepkg.sh.in
}

build() {
	cd pacman-$pkgver
	./configure \
		--prefix=/tools \
		--sysconfdir=/tools/etc \
		--localstatedir=/var \
		--build=$XHOST \
		--host=$XTARGET \
		--with-scriptlet-shell=/bin/bash \
		--disable-doc \
		--disable-nls \
		DUFLAGS="-sk" \
		SEDINPLACEFLAGS="-i"
	make
}

package() {
	cd pacman-$pkgver
	make DESTDIR=$pkgdir install

	mkdir -p $pkgdir/output/{packages,sources}

	install -dm755 $pkgdir/tools/etc
	install -m644 $srcdir/makepkg.conf $pkgdir/tools/etc
	install -m644 $srcdir/pacman.conf $pkgdir/tools/etc

	sed -i 's/CheckSpace/#CheckSpace/' $pkgdir/tools/etc/pacman.conf

	sed -i $pkgdir/tools/etc/makepkg.conf \
		-e "s|@CARCH[@]|$CARCH|g" \
		-e "s|@CHOST[@]|$CHOST|g" \
		-e "s|@CFLAGS[@]|$CFLAGS|g" \
		-e "s|@CXXFLAGS[@]|$CXXFLAGS|g" \
		-e "s|@PKGS[@]|/output/packages|g" \
		-e "s|@SOURCES[@]|/output/sources|g" \
		-e "s|@GCCOPTS[@]|$GCCOPTS|g" \
		-e "s|@BINUTILSOPTS[@]|$BINUTILSOPTS|g" \
		-e "s|@MKOPTS[@]|$MAKEFLAGS|g" \
		-e "s|@LC_ALL[@]|$LC_ALL|g"

	sed -i $pkgdir/tools/etc/pacman.conf \
		-e "s|@CARCH[@]|$CARCH|g" \
		-e "s|@PACKAGES[@]|$PACKAGES|g"
}
