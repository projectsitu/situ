pkgname=chroot-gcc-pass2
pkgver=8+20181207
snapver=8.2.1
gmpver=6.1.2
mpfrver=4.0.1
mpcver=1.1.0
islver=0.20
pkgrel=1
arch=('x86_64' 'i686' 'aarch64' 'armv7h' 'armv5tel' 'mips64' 'mips' 'ppc64le' 'ppc64' 'ppc')
provides=('gcc' 'gcc-libs')
replaces=('chroot-gcc')
source=(http://mirror.linux-ia64.org/gnu/gcc/snapshots/${pkgver/+/-}/gcc-${pkgver/+/-}.tar.xz
	http://ftpmirror.gnu.org/gnu/gmp/gmp-$gmpver.tar.xz
	http://www.mpfr.org/mpfr-$mpfrver/mpfr-$mpfrver.tar.xz
	http://ftpmirror.gnu.org/gnu/mpc/mpc-$mpcver.tar.gz
	http://isl.gforge.inria.fr/isl-$islver.tar.xz
	0001-Fix-stack-protection-issues.patch
	openmp-vectorize-v2.patch
	fortran-vector-v2.patch
	optimize.patch
	ipa-cp.patch
	optimize-at-least-some.patch
	avx512-when-we-ask-for-it.patch
	hj.patch
	pow-optimization.patch
	hj-patch-round.patch
	cpu_indicator.patch
	gcc-4.9-musl-fortify.patch
	gcc-6.1-musl-libssp.patch
	libgcc-always-build-gcceh.a.patch
	posix_memalign.patch
	gcc-pure64.patch
	gcc-pure64-mips.patch)

prepare() {
	cd gcc-${pkgver/+/-}
	sed -i 's@\./fixinc\.sh@-c true@' gcc/Makefile.in

	# some performance
	patch -p1 -i $srcdir/0001-Fix-stack-protection-issues.patch
	patch -p1 -i $srcdir/openmp-vectorize-v2.patch
	patch -p1 -i $srcdir/fortran-vector-v2.patch
	patch -p1 -i $srcdir/optimize.patch
	patch -p1 -i $srcdir/ipa-cp.patch
	patch -p1 -i $srcdir/optimize-at-least-some.patch
	patch -p1 -i $srcdir/avx512-when-we-ask-for-it.patch
	patch -p1 -i $srcdir/hj.patch
	patch -p1 -i $srcdir/pow-optimization.patch
	patch -p1 -i $srcdir/hj-patch-round.patch

	# musl fixes
	patch -Np1 -i $srcdir/cpu_indicator.patch
	patch -Np1 -i $srcdir/gcc-4.9-musl-fortify.patch
	patch -Np1 -i $srcdir/gcc-6.1-musl-libssp.patch
	patch -Np1 -i $srcdir/libgcc-always-build-gcceh.a.patch
	patch -Np1 -i $srcdir/posix_memalign.patch

	# no multilib
	patch -Np1 -i $srcdir/gcc-pure64.patch
	patch -Np1 -i $srcdir/gcc-pure64-mips.patch

	for file in gcc/config/linux.h gcc/config/i386/linux.h gcc/config/i386/linux64.h \
			gcc/config/arm/linux-eabi.h gcc/config/arm/linux-elf.h \
			gcc/config/aarch64/aarch64-linux.h \
			gcc/config/rs6000/linux64.h gcc/config/rs6000/sysv4.h \
			gcc/config/mips/linux.h; do
		cp -u $file{,.orig}
		sed -e 's@/lib\(64\)\?\(32\)\?/ld@/tools&@g' \
			-e 's@/usr@/tools@g' $file.orig > $file
		echo '
#undef STANDARD_STARTFILE_PREFIX_1
#undef STANDARD_STARTFILE_PREFIX_2
#define STANDARD_STARTFILE_PREFIX_1 "/tools/lib/"
#define STANDARD_STARTFILE_PREFIX_2 ""' >> $file
		touch $file.orig
done
	mv ../gmp-$gmpver gmp
	mv ../mpfr-$mpfrver mpfr
	mv ../mpc-$mpcver mpc
	mv ../isl-$islver isl

	mkdir build
}

build() {
	cd gcc-${pkgver/+/-}
	cd build
	../configure \
		--prefix=/tools \
		--build=$CHOST $GCCOPTS \
		--with-local-prefix=/tools \
		--with-native-system-header-dir=/tools/include \
		--with-system-zlib \
		--enable-__cxa_atexit \
		--enable-bootstrap \
		--enable-checking=release \
		--enable-clocale=generic \
		--enable-default-pie \
		--enable-default-ssp \
		--enable-install-libiberty \
		--enable-languages=c,c++ \
		--enable-linker-build-id \
		--enable-lto \
		--enable-plugin \
		--enable-shared \
		--enable-threads=posix \
		--enable-tls \
		--disable-gnu-indirect-function \
		--disable-libmpx \
		--disable-libmudflap \
		--disable-libsanitizer \
		--disable-libssp \
		--disable-libstdcxx-pch \
		--disable-multilib \
		--disable-nls \
		--disable-symvers \
		libat_cv_have_ifunc=no
	make profiledbootstrap
}

package() {
	cd gcc-${pkgver/+/-}
	cd build
	make DESTDIR=$pkgdir install

	ln -sf gcc $pkgdir/tools/bin/cc

	ln -sf ../bin/cpp $pkgdir/tools/lib

	install -dm755 $pkgdir/tools/lib/bfd-plugins
	ln -sf /tools/libexec/gcc/$CHOST/$snapver/liblto_plugin.so $pkgdir/tools/lib/bfd-plugins/
}
